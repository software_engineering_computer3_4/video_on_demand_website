# 视频点播网站

#### Description
本系统主要目标是实现用户点播视频，用户可以不看广告，不为某个节目赶时间，随时直接点播希望收看的内容，随时搜索、上传、下载、收藏、评论视频。网站管理人员只需进行信息维护，审核视频的合法性，将视频信息的更新交给了用户。系统每天会根据视频的上传时间、热度、用户喜好为用户推荐视频。系统包括2种角色：普通用户和管理员。普通用户可以利用本系统浏览视频、播放视频、评论视频、上传视频、下载视频、收藏视频、举报视频，并且可以与其他用户互动。管理员功能可以进行视频分类和视频信息管理、审核视频、用户信息管理、用户权限分配等。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
