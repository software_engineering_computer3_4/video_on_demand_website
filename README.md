# 视频点播网站

#### 介绍
本系统主要目标是实现用户点播视频，用户可以不看广告，不为某个节目赶时间，随时直接点播希望收看的内容，随时搜索、上传、下载、收藏、评论视频。网站管理人员只需进行信息维护，审核视频的合法性，将视频信息的更新交给了用户。系统每天会根据视频的上传时间、热度、用户喜好为用户推荐视频。系统包括2种角色：普通用户和管理员。普通用户可以利用本系统浏览视频、播放视频、评论视频、上传视频、下载视频、收藏视频、举报视频，并且可以与其他用户互动。管理员功能可以进行视频分类和视频信息管理、审核视频、用户信息管理、用户权限分配等。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
